import graphene
import DonerInfo.schema 

class Query(DonerInfo.schema.Query,graphene.ObjectType):
    pass

    
class Mutation(DonerInfo.schema.Mutation,graphene.ObjectType):
    pass

schema = graphene.Schema(query=Query, mutation= Mutation)