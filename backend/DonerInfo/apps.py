from django.apps import AppConfig


class DonerinfoConfig(AppConfig):
    name = 'DonerInfo'
