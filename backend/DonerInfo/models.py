from django.db import models


class Doner(models.Model):
    Donerid = models.CharField(max_length=100)
    Name = models.CharField(max_length=100)
    PHNumber = models.CharField(max_length=100)
    Photo = models.ImageField(
        blank = True, null= True,
        upload_to='cover/%Y/%m/%D'
    )
   
    Address = models.CharField(max_length = 100)
    Address1= models.CharField(max_length=100)
    BloodGroup = models.CharField(max_length=100)
    def __str__(self):
        return self.Donerid

class Donation(models.Model):
    DoneridD = models.CharField(max_length=100)
    Donationid = models.CharField(max_length=100)
    TVideo = models.CharField(max_length=100)
    DonateType = models.CharField(max_length=100)
    DateofDonation = models.CharField(max_length=100)
    ProcessState = models.CharField(max_length=100)


    def __str__(self):
        return self.Donationid
