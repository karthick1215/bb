import graphene
from graphene_django.types import DjangoObjectType
from DonerInfo.models import Donation, Doner
from graphene_django.filter.fields import DjangoFilterConnectionField
from django.db import models


class Create_donation(graphene.Mutation):
    id = graphene.Int()
    DoneridD = graphene.String()
    Donationid = graphene.String()
    TVideo = graphene.Boolean()
    DonateType = graphene.String()
    DateofDonation = graphene.String()
    ProcessState = graphene.String()
    class Arguments:
        DoneridD = graphene.String()
        Donationid = graphene.String()
        TVideo = graphene.Boolean()
        DonateType = graphene.String()
        DateofDonation = graphene.String()
        ProcessState = graphene.String()

    def mutate(self, info , DoneridD,Donationid, TVideo ,DonateType ,DateofDonation , ProcessState):
        doner = Donation(DoneridD =  DoneridD ,Donationid = Donationid ,TVideo = TVideo ,DonateType = DonateType ,DateofDonation = DateofDonation, ProcessState = ProcessState) 
        doner.save()
        return Create_donation(DoneridD = doner.DoneridD)

class DonationType(DjangoObjectType):
    class Meta:
        model = Donation
        filter_fields = ['DoneridD']
        interfaces = (graphene.Node, )


class DonerType(DjangoObjectType):
    class Meta:
        model = Doner
        filter_fields= ['Donerid']
        interfaces = (graphene.Node, )
        

class Query(object):
    all_Doner = DjangoFilterConnectionField(DonerType)
    all_Donation = DjangoFilterConnectionField(DonationType)
        
    def resolve_all_Doner(self, info, **kwargs):
        return Doner.objects.all()
        
    def resolve_all_Donation(self, info, **kwargs):
        return Donation.objects.all()


class Mutation(graphene.ObjectType):
    create_donation = Create_donation.Field()